import 'package:app_api_member_manager/config/config_api.dart';
import 'package:app_api_member_manager/functions/token_lib.dart';
import 'package:app_api_member_manager/model/common_result.dart';
import 'package:app_api_member_manager/model/date_search_request.dart';
import 'package:app_api_member_manager/model/login/login_response.dart';
import 'package:app_api_member_manager/model/workTimeRecord/work_time_record_list_result.dart';
import 'package:app_api_member_manager/model/workTimeRecord/work_time_record_state_result.dart';
import 'package:dio/dio.dart';

class RepoWorkTimeRecord {

  Future<WorkTimeRecordStateResult> getMyState() async {
    const String baseUrl = '$apiUrl/work-time/user/search/{memberId}';

    Dio dio = Dio();

    LoginResponse? token = await TokenLib.getToken();

    final response = await dio.get(
      baseUrl.replaceAll('{memberId}', token!.memberId.toString()),
      options: Options(
        followRedirects: false,
        validateStatus: (status) => (status == 200),
      ),
    );

    var test = response.data;

    return WorkTimeRecordStateResult.fromJson(response.data);
  }

  Future<CommonResult> putWorkTimeRecord(String workState) async {
    const String baseUrl = '$apiUrl/work-time/user/status/{workState}/member-id/{memberId}';

    Dio dio = Dio();

    LoginResponse? token = await TokenLib.getToken();

    try {
      final response = await dio.put(
        baseUrl.replaceAll('{memberId}', token!.memberId.toString()).replaceAll('{workState}', workState),
        options: Options(
          followRedirects: false,
        ),
      );

      return CommonResult.fromJson(response.data);
    } on DioError catch (e) {
      return CommonResult.fromJson(e.response!.data);
    }
  }

  Future<WorkTImeRecordListResult> getWorkTimeRecordList(int year, int month) async {
    const String baseUrl = '$apiUrl/work-time/user/search';

    Dio dio = Dio();

    LoginResponse? token = await TokenLib.getToken();
    DateSearchRequest searchRequest = DateSearchRequest(token!.memberId, year, month);

    final response = await dio.post(
      baseUrl,
      data: searchRequest.toJson(),
      options: Options(
        followRedirects: false,
        validateStatus: (status) => (status == 200),
      ),
    );

    return WorkTImeRecordListResult.fromJson(response.data);
  }


}