import 'package:app_api_member_manager/config/config_api.dart';
import 'package:app_api_member_manager/functions/token_lib.dart';
import 'package:app_api_member_manager/model/common_result.dart';
import 'package:app_api_member_manager/model/date_search_request.dart';
import 'package:app_api_member_manager/model/login/login_response.dart';
import 'package:app_api_member_manager/model/vacation/vacation_apply_request.dart';
import 'package:app_api_member_manager/model/vacation/vacation_result.dart';
import 'package:dio/dio.dart';

class RepoVacation {

  Future<CommonResult> setVacation(VacationApplyRequest request) async {
    const String baseUrl = '$apiUrl/vacation/user/new/member-id/{memberId}';

    Dio dio = Dio();

    LoginResponse? token = await TokenLib.getToken();

    final response = await dio.post(
      baseUrl.replaceAll('{memberId}', token!.memberId.toString()),
      data: request.toJson(),
      options: Options(
          followRedirects: false
      ),
    );

    return CommonResult.fromJson(response.data);
  }


  Future<VacationResult> getMyVacationList(int year, int month) async {
    const String baseUrl = '$apiUrl/vacation/user/search';

    Dio dio = Dio();

    LoginResponse? token = await TokenLib.getToken();
    DateSearchRequest searchRequest = DateSearchRequest(token!.memberId, year, month);

    final response = await dio.post(
      baseUrl,
      data: searchRequest.toJson(),
      options: Options(
        followRedirects: false,
        validateStatus: (status) => (status == 200),
      ),
    );

    return VacationResult.fromJson(response.data);
  }


}