import 'package:app_api_member_manager/config/config_api.dart';
import 'package:app_api_member_manager/functions/token_lib.dart';
import 'package:app_api_member_manager/model/common_result.dart';
import 'package:app_api_member_manager/model/login/login_request.dart';
import 'package:app_api_member_manager/model/login/login_response.dart';
import 'package:app_api_member_manager/model/login/login_result.dart';
import 'package:app_api_member_manager/model/member/member_detail_result.dart';
import 'package:app_api_member_manager/model/member/member_password_change_request.dart';
import 'package:app_api_member_manager/model/member/member_put_request.dart';
import 'package:dio/dio.dart';

class RepoMember {
  Future<LoginResult> doLogin(LoginRequest loginRequest) async {
    const String baseUrl = '$apiUrl/member/user/login';

    Dio dio = Dio();

    final response = await dio.post(
        baseUrl,
        data: loginRequest.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return LoginResult.fromJson(response.data);


  }

  Future<MemberDetailResult> getMemberDetail () async {
    const String baseUrl = '$apiUrl/member/user/search/member-id/{memberId}';

    Dio dio = Dio();

    LoginResponse? token = await TokenLib.getToken();

    final response = await dio.get(
      baseUrl.replaceAll('{memberId}', token!.memberId.toString()),
      options: Options(
        followRedirects: false,
        validateStatus: (status) => (status == 200),
      ),
    );

    return MemberDetailResult.fromJson(response.data);
  }



  Future<CommonResult> putMember(MemberPutRequest request) async {
    const String baseUrl = '$apiUrl/member/user/new/{memberId}';

    Dio dio = Dio();

    LoginResponse? token = await TokenLib.getToken();

    final response = await dio.put(
        baseUrl.replaceAll('{memberId}', token!.memberId.toString()),
        data: request.toJson(),
        options: Options(
            followRedirects: false
        ),
    );

    return CommonResult.fromJson(response.data);
  }


  Future<CommonResult> putPassword (MemberPasswordChangeRequest changeRequest) async {
    const String baseUrl = '$apiUrl/member/user/password/{memberId}';

    Dio dio = Dio();

    LoginResponse? token = await TokenLib.getToken();

    try {
      final response = await dio.put(
        baseUrl.replaceAll('{memberId}', token!.memberId.toString()),
        data: changeRequest.toJson(),
        options: Options(
          followRedirects: false,
        ),
      );

      return CommonResult.fromJson(response.data);
    } on DioError catch (e) {
      return CommonResult.fromJson(e.response!.data);
    }
  }





}