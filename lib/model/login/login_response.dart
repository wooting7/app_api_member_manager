class LoginResponse {
  int memberId;
  String username;
  String memberName;
  String department;

  LoginResponse(this.memberId, this.username, this.memberName, this.department);


  factory LoginResponse.fromJson(Map<String, dynamic> json) {
    return LoginResponse(
      json['memberId'],
      json['username'],
      json['memberName'],
      json['department']
    );
  }
}