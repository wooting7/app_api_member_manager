class MemberPasswordChangeRequest {
  String password;
  String newPassword;
  String newPasswordConfirm;

  MemberPasswordChangeRequest(this.password, this.newPassword, this.newPasswordConfirm);

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = <String, dynamic> {};

    data['password'] = this.password;
    data['newPassword'] = this.newPassword;
    data['newPasswordConfirm'] = this.newPasswordConfirm;

    return data;
  }
}