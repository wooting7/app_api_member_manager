import 'package:app_api_member_manager/model/member/member_detail.dart';

class MemberDetailResult {
  MemberDetail data;
  bool isSuccess;
  int code;
  String msg;

  MemberDetailResult(this.data, this.isSuccess, this.code, this.msg);

  factory MemberDetailResult.fromJson(Map<String, dynamic> json) {
    return MemberDetailResult(
      MemberDetail.fromJson(json['data']),
      json['isSuccess'],
      json['code'],
      json['msg'],
    );
  }
}