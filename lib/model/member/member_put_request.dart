

class MemberPutRequest {
   String phoneNumber;
   String finalEducation;
   String emergencyContactNumber;
   String emergencyContactName;
   String emergencyContactRelationship;
   String address;

   MemberPutRequest(
   this.phoneNumber,
   this.finalEducation,
   this.emergencyContactNumber,
   this.emergencyContactName,
   this.emergencyContactRelationship,
   this.address);

   Map<String, dynamic> toJson() {
     final Map<String, dynamic> data = Map<String, dynamic>();

     data['phoneNumber'] = this.phoneNumber;
     data['finalEducation'] = this.finalEducation;
     data['emergencyContactNumber'] = this.emergencyContactNumber;
     data['emergencyContactName'] = this.emergencyContactName;
     data['emergencyContactRelationship'] = this.emergencyContactRelationship;
     data['address'] = this.address;

     return data;
   }



}