

class MemberDetail {
  int id;
  String username;
  String password;
  String memberName;
  String department;
  String position;
  String birthday;
  String dateJoin;
  String phoneNumber;
  String finalEducation;
  String emergencyContactNumber;
  String emergencyContactName;
  String emergencyContactRelationship;
  String address;

  MemberDetail(this.id,
      this.username,
      this.password,
      this.memberName,
      this.department,
      this.position,
      this.birthday,
      this.dateJoin,
      this.phoneNumber,
      this.finalEducation,
      this.emergencyContactNumber,
      this.emergencyContactName,
      this.emergencyContactRelationship,
      this.address);

  factory MemberDetail.fromJson(Map<String, dynamic> json) {
    return MemberDetail(
      json['id'],
      json['username'],
      json['password'],
      json['memberName'],
      json['department'],
      json['position'],
      json['birthday'],
      json['dateJoin'],
      json['phoneNumber'] ?? '-',
      json['finalEducation'] ?? '-',
      json['emergencyContactNumber'] ?? '-',
      json['emergencyContactName'] ?? '-',
      json['emergencyContactRelationship'] ?? '-',
      json['address'] ?? '-',

    );
  }
}