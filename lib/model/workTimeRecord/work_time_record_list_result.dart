import 'package:app_api_member_manager/model/workTimeRecord/work_time_record_item.dart';

class WorkTImeRecordListResult {
  List<WorkTimeRecordItem> list;
  bool isSuccess;
  int code;
  String msg;
  int totalItemCount;

  WorkTImeRecordListResult(this.list, this.isSuccess, this.code, this.msg, this.totalItemCount);

  factory WorkTImeRecordListResult.fromJson(Map<String, dynamic> json) {
    return WorkTImeRecordListResult(
      (json['list'] as List).map((e) => WorkTimeRecordItem.fromJson(e)).toList(),
      json['isSuccess'],
      json['code'],
      json['msg'],
      json['totalItemCount'],
    );
  }
}