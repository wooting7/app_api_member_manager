class WorkTimeRecordStateResponse {
  String workState;
  String workStateName;
  String? dateStart;
  String? dateEarlyLeave;
  String? dateEnd;

  WorkTimeRecordStateResponse(this.workState, this.workStateName, this.dateStart, this.dateEarlyLeave, this.dateEnd);

  factory WorkTimeRecordStateResponse.fromJson(Map<String, dynamic> json) {
    return WorkTimeRecordStateResponse(
      json['workState'],
      json['workStateName'],
      json['dateStart'],
      json['dateEarlyLeave'],
      json['dateEnd'],

      //todo null값 예외처리 해서 연동할것, 가독성 떨어짐
    );
  }
}