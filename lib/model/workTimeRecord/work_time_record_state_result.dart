
import 'package:app_api_member_manager/model/workTimeRecord/work_time_record_state_response.dart';

class WorkTimeRecordStateResult {
  WorkTimeRecordStateResponse data;
  bool isSuccess;
  int code;
  String msg;

  WorkTimeRecordStateResult(this.data, this.isSuccess, this.code, this.msg);

  factory WorkTimeRecordStateResult.fromJson(Map<String, dynamic> json) {
    return WorkTimeRecordStateResult(
      WorkTimeRecordStateResponse.fromJson(json['data']),
      json['isSuccess'],
      json['code'],
      json['msg'],
    );
  }
}