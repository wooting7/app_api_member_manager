class WorkTimeRecordItem {
  int memberId;
  DateTime dateWork;
  String workState;
  String dateStart;
  String dateEarlyLeave;
  String dateEnd;

  WorkTimeRecordItem(
    this.memberId,
    this.dateWork,
    this.workState,
    this.dateStart,
    this.dateEarlyLeave,
    this.dateEnd,
  );

  factory WorkTimeRecordItem.fromJson(Map<String, dynamic> json) {
    return WorkTimeRecordItem(
      json['memberId'],
      DateTime.parse(json['dateWork']),
      json['workState'],
      json['dateStart'],
      json['dateEarlyLeave'] ?? '-',
      json['dateEnd'] ?? '-',
    );
  }
}
