
class VacationApplyRequest {

  String timeStart;
  String timeEnd;
  String dateRequest;
  String vacationName;
  String detail;

  VacationApplyRequest(
      this.timeStart,
      this.timeEnd,
      this.dateRequest,
      this.vacationName,
      this.detail
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['timeStart'] = this.timeStart;
    data['timeEnd'] = this.timeEnd;
    data['dateRequest'] = this.dateRequest;
    data['vacationName'] = this.vacationName;
    data['detail'] = this.detail;

    return data;
  }
}