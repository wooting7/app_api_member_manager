class VacationItem {
   int id;
   int memberId;
   String timeStart;
   String timeEnd;
   DateTime dateRequest;
   String vacationName;
   String detail;
   String approveState;


   VacationItem(
       this.id,
       this.memberId,
       this.timeStart,
       this.timeEnd,
       this.dateRequest,
       this.vacationName,
       this.detail,
       this.approveState,
);

   factory VacationItem.fromJson(Map<String, dynamic> json) {
      return VacationItem(
         json['id'],
         json['memberId'],
         json['timeStart'],
         json['timeEnd'],
         DateTime.parse(json['dateRequest']),
         json['vacationName'],
         json['detail'],
         json['approveState'],

      );
   }
}