
import 'package:app_api_member_manager/model/vacation/vacation_item.dart';

class VacationResult {
  List<VacationItem> list;
  bool isSuccess;
  int code;
  String msg;
  int totalItemCount;

  VacationResult(this.list, this.isSuccess, this.code, this.msg, this.totalItemCount);

  factory VacationResult.fromJson(Map<String, dynamic> json) {
    return VacationResult(
      (json['list'] as List).map((e) => VacationItem.fromJson(e)).toList(),
      json['isSuccess'],
      json['code'],
      json['msg'],
      json['totalItemCount'],
    );
  }
}