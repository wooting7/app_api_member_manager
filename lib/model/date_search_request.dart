class DateSearchRequest {
  int memberId;
  int dateStandardYear;
  int dateStandardMonth;

  DateSearchRequest(this.memberId, this.dateStandardYear, this.dateStandardMonth);

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = <String, dynamic> {};

    data['memberId'] = this.memberId;
    data['dateStandardYear'] = this.dateStandardYear;
    data['dateStandardMonth'] = this.dateStandardMonth;

    return data;
  }
}