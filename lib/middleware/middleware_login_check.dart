import 'package:app_api_member_manager/functions/token_lib.dart';
import 'package:app_api_member_manager/model/login/login_response.dart';
import 'package:app_api_member_manager/page/page_index.dart';
import 'package:app_api_member_manager/page/page_login.dart';
import 'package:flutter/material.dart';

class MiddlewareLoginCheck {
  void check(BuildContext context) async {
    LoginResponse? token = await TokenLib.getToken();

    /*
    최초에 앱을 실행하면 token은 null이고
    한번이라도 로그아웃을 했다면 빈문자열임. (로그아웃 시 setToken에서 ''로 삭제해버림)
    token이 없으면 비회원용 메인으로 강제이동
    token이 있으면 회원용 메인으로 강제이동
     */
    if (token == null || token.memberId == 0 || token.username == '' || token.memberName == '' || token.department == '') {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageLogin()), (route) => false);
    } else {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageIndex()), (route) => false);
    }
  }
}