import 'package:app_api_member_manager/component/component_notification.dart';
import 'package:app_api_member_manager/model/login/login_response.dart';
import 'package:app_api_member_manager/page/page_login.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

/*
SharedPreferences 에 데이터가 저장되는 방식은 Map임을 반드시 숙지하고 갈 것.
Map이 무엇인지 알아야 함.
 */
class TokenLib {
  // token 가져오기
  static Future<LoginResponse?> getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String? memberId = prefs.getString('memberId');
    String? username = prefs.getString('username');
    String? memberName = prefs.getString('memberName');
    String? department = prefs.getString('department');

    if (memberId == null || username == null || memberName == null || department == null) {
      return null;
    } else {
      return LoginResponse(int.parse(memberId), username, memberName, department);
    }
  }

  // token 세팅
  static void setToken(LoginResponse response) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('memberId', response.memberId.toString());
    prefs.setString('username', response.username);
    prefs.setString('memberName', response.memberName);
    prefs.setString('department', response.department);
  }

  // 로그아웃 처리
  static void logout(BuildContext context) async {
    // 기존에 닫혀있던 토스트팝업, 커스텀로딩 다 닫고
    BotToast.closeAllLoading();

    // 메모리에 접근해서 key 가 token인 값을 빈문자열('')로 바꿔버리고
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('memberId', '0');
    prefs.setString('username', '');
    prefs.setString('memberName', '');
    prefs.setString('department', '');

    // 토스트메시지 띄우고
    ComponentNotification(
      success: false,
      title: '로그아웃',
      subTitle: '로그아웃되어 비회원용 메인페이지로 이동합니다.',
    ).call();

    // 지금까지 이동했던 모든 페이지 내역들 싹 다 지우고 (비회원용 메인)으로 강제로 이동시킨다.
    Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageLogin()), (route) => false);
  }
}