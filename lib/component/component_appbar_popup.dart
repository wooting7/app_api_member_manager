import 'package:app_api_member_manager/config/config_color.dart';
import 'package:flutter/material.dart';

class ComponentAppbarPopup extends StatelessWidget with PreferredSizeWidget {
  const ComponentAppbarPopup({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: false,
      automaticallyImplyLeading: false,
      backgroundColor: colorWhite,
      title: Text(
        title, style: TextStyle(color: colorDarkGray),
      ),
      elevation: 1,
      actions: [
        IconButton(
            onPressed: () => Navigator.of(context).pop(),
            icon: const Icon(Icons.clear)),
      ],
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(45);
}
