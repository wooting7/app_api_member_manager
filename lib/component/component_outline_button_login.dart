import 'package:app_api_member_manager/config/config_color.dart';
import 'package:flutter/material.dart';

class ComponentOutlineButtonLogin extends StatelessWidget {
  const ComponentOutlineButtonLogin({super.key, required this.callback, required this.name});

  final VoidCallback callback;
  final String name;

  @override
  Widget build(BuildContext context) {
    return TextButton(
        onPressed: callback,

        child: Text(name, style: TextStyle(color:colorWhite )),


        );


  }
}
