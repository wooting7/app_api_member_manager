import 'package:app_api_member_manager/config/config_color.dart';
import 'package:app_api_member_manager/config/config_decoration.dart';
import 'package:app_api_member_manager/model/workTimeRecord/work_time_record_item.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ComponentWorkTimeRecordListItem extends StatelessWidget {
  const ComponentWorkTimeRecordListItem({
    super.key,
    required this.item,
  });

  final WorkTimeRecordItem item;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: bodyPaddingAll,
      margin: bodyPaddingAll,
      decoration: BoxDecoration(
        color: colorWhite,
        border: Border.all(color: colorPrimary),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Row(
            children: [
              Text('날짜 : '),
              Text(
                DateFormat('yy-MM-dd').format(item.dateWork),
              ),
              SizedBox(
                width: 30,
              ),
              if (item.workState == 'COMPANY_IN')
                Text('상태 : 출근', style: TextStyle(color: colorPrimary)),
              if (item.workState == 'EARLY_LEAVE')
                Text('상태 : 조퇴', style: TextStyle(color: colorPrimary)),
              if (item.workState == 'COMPANY_OUT')
                Text('상태 : 퇴근', style: TextStyle(color: colorPrimary)),
            ],
          ),
          Row(
            children: [
              Text('출근 : '),
              Text(item.dateStart, style: TextStyle(color: colorGray)),
              SizedBox(
                width: 30,
              ),
              if (item.dateEarlyLeave == '-' &&
                  item.dateEnd != '-') // 조퇴아니고 퇴근시간있으면item.dateEnd,
                Text('퇴근 : '),
              Text(item.dateEnd, style: TextStyle(color: colorGray)),
              if ((item.dateEnd == '-') &&
                  (item.dateEarlyLeave != '-')) // 퇴근아니고 조퇴시간 있으면
                Text('조퇴 : '),
              Text(item.dateEarlyLeave, style: TextStyle(color: colorGray)),
            ],
          ),

        ],
      ),
    );
  }
}
