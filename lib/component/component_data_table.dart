import 'package:app_api_member_manager/config/config_color.dart';
import 'package:app_api_member_manager/config/config_decoration.dart';
import 'package:flutter/material.dart';

class ComponentDataTable extends StatelessWidget {
  const ComponentDataTable({
    super.key,
 //   required this.item,
    required this.callback
  });

//  final DrugListItem item;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        DataTable(
          columns: [
            DataColumn(label: Text('날짜')),
            DataColumn(label: Text('근태')),
            DataColumn(label: Text('결제')),
          ],
          rows: [
            DataRow(cells: [
              DataCell(Text('10-01')),
              DataCell(Text('연차')),
              DataCell(Text('대기중')),
            ]),
            DataRow(cells: [
              DataCell(Text('10-01')),
              DataCell(Text('연차')),
              DataCell(Text('대기중')),
            ]),
          ],
        ),
      ],
    );
  }
}

