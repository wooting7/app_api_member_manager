import 'package:app_api_member_manager/config/config_color.dart';
import 'package:app_api_member_manager/config/config_decoration.dart';
import 'package:app_api_member_manager/model/vacation/vacation_item.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ComponentVacationListItem extends StatelessWidget {
  const ComponentVacationListItem({
    super.key,
    required this.item,
  });

  final VacationItem item;

  @override
  Widget build(BuildContext context) {
    return  Container(
      padding: bodyPaddingAll,
      margin: bodyPaddingAll,
      decoration: BoxDecoration(
        color: colorWhite,
        borderRadius: BorderRadius.circular(10),
      ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Row(
              children: [
                Text(DateFormat('yy-MM-dd').format(item.dateRequest), style: TextStyle(color: colorGray)),
                SizedBox(width: 30,),
                Text(item.vacationName == 'VACATION' ? '연차' : '반차', style: TextStyle(color: colorGray)),
                SizedBox(width: 30,),
                if (item.approveState == 'STAND_BY')
                Text('대기', style: TextStyle(color: colorGray)),
                if (item.approveState == 'APPROVE')
                  Text('승인',style: TextStyle(color: colorPrimary),),
                if (item.approveState == 'REJECT')
                  Text('반려', style: TextStyle(color: colorRed)),
                if (item.approveState == 'CANCEL')
                  Text('회수', style: TextStyle(color: colorGray)),
              ],
            ),
            Divider()


              ],
        ),
      );

  }
}