import 'package:app_api_member_manager/config/config_color.dart';
import 'package:app_api_member_manager/config/config_size.dart';
import 'package:flutter/material.dart';

class ComponentMemberText extends StatelessWidget {
  const ComponentMemberText({super.key,   required this.name,
    required this.variableName});

  final String name;
  final String variableName;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Icon(Icons.circle, size: 12,),
            SizedBox(width: 10,),
            Text(name, style: TextStyle()),
          ],
        ),
        Row(
          children: [
            const SizedBox(height: 10, width: 22,),
            Text(variableName, style: TextStyle(color: colorGray)),
          ],
        ),
        Divider()

      ],
    );
  }
}
