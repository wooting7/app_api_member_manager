import 'package:app_api_member_manager/component/component_appbar_popup.dart';
import 'package:app_api_member_manager/component/component_custom_loading.dart';
import 'package:app_api_member_manager/component/component_notification.dart';
import 'package:app_api_member_manager/component/component_outline_button.dart';
import 'package:app_api_member_manager/config/config_color.dart';
import 'package:app_api_member_manager/config/config_decoration.dart';
import 'package:app_api_member_manager/config/config_form_validator.dart';
import 'package:app_api_member_manager/model/member/member_put_request.dart';
import 'package:app_api_member_manager/repository/repo_member.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PageMemberPut extends StatefulWidget {
  PageMemberPut({super.key, this.memberPutRequest});

  MemberPutRequest? memberPutRequest;

  @override
  State<PageMemberPut> createState() => _PageMemberPutState();
}

class _PageMemberPutState extends State<PageMemberPut> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _putMember(MemberPutRequest request) async {
    // 여기서부터
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    // 여기까지 커스텀로딩 띄우는 소스

    await RepoMember().putMember(request).then((res) {
      BotToast.closeAllLoading(); // 커스텀로딩 닫고

      ComponentNotification(
        success: true,
        title: '회원 수정 성공',
        subTitle: res.msg, // 백엔드에서 반환받은 메세지 넣기
      ).call();

      // 창 닫으면서 다 처리하고 정상적으로 닫혔다!! 라고 알려주기
      Navigator.pop(context, [true] // <- 이부분.. 다 처리하고 정상적으로 닫혔다!!
          );
    }).catchError((err) {
      BotToast.closeAllLoading(); // 커스텀로딩 닫고

      ComponentNotification(
        success: false,
        title: '회원 수정 실패',
        subTitle: '회원 수정에 실패하였습니다.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(
        title: '내정보 수정',
      ),
      body: _buildBody(),
      backgroundColor: colorSecondary,
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
        child: Container(
      padding: bodyPaddingAll,
      margin: bodyPaddingAll,
      decoration: BoxDecoration(
        color: colorWhite,
        borderRadius: BorderRadius.circular(10),
      ),
      child: FormBuilder(
      key: _formKey,
      autovalidateMode: AutovalidateMode.disabled,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          FormBuilderTextField(
            name: 'phoneNumber',
            decoration: const InputDecoration(
                labelText: '전화번호', hintText: '13자 입력해주세요'),
            validator: FormBuilderValidators.compose([
              FormBuilderValidators.equalLength(13,
                  errorText: formErrorEqualLength(13)),
            ]),
            keyboardType: TextInputType.number,
          ),
          const SizedBox(
            height: 10,
          ),
          FormBuilderTextField(
            name: 'finalEducation',
            decoration: const InputDecoration(
                labelText: '최종학력', hintText: '2~10자 입력해주세요'),
            validator: FormBuilderValidators.compose([
              FormBuilderValidators.minLength(2,
                  errorText: formErrorMinLength(2)),
              FormBuilderValidators.maxLength(10,
                  errorText: formErrorMaxLength(10)),
            ]),
            keyboardType: TextInputType.text,
          ),
          const SizedBox(
            height: 10,
          ),
          FormBuilderTextField(
            name: 'address',
            decoration: const InputDecoration(
                labelText: '집주소', hintText: '10~50자 입력해주세요'),
            validator: FormBuilderValidators.compose([
              FormBuilderValidators.minLength(10,
                  errorText: formErrorMinLength(10)),
              FormBuilderValidators.maxLength(50,
                  errorText: formErrorMaxLength(50)),
            ]),
            keyboardType: TextInputType.text,
          ),
          const SizedBox(
            height: 10,
          ),
          FormBuilderTextField(
            name: 'emergencyContactNumber',
            decoration: const InputDecoration(
                labelText: '비상연락망', hintText: '10~30자 입력해주세요'),
            validator: FormBuilderValidators.compose([
              FormBuilderValidators.minLength(10,
                  errorText: formErrorMinLength(10)),
              FormBuilderValidators.maxLength(30,
                  errorText: formErrorMaxLength(30)),
            ]),
            keyboardType: TextInputType.text,
          ),
          const SizedBox(
            height: 10,
          ),
          FormBuilderTextField(
            name: 'emergencyContactName',
            decoration: const InputDecoration(
                labelText: '비상연락망 성함', hintText: '2~20자 입력해주세요'),
            validator: FormBuilderValidators.compose([
              FormBuilderValidators.minLength(2,
                  errorText: formErrorMinLength(2)),
              FormBuilderValidators.maxLength(20,
                  errorText: formErrorMaxLength(20)),
            ]),
            keyboardType: TextInputType.text,
          ),
          const SizedBox(
            height: 10,
          ),
          FormBuilderTextField(
            name: 'emergencyContactRelationship',
            decoration: const InputDecoration(
                labelText: '비상연락망 관계', hintText: '2~10자 입력해주세요'),
            validator: FormBuilderValidators.compose([
              FormBuilderValidators.required(errorText: formErrorRequired),
              FormBuilderValidators.minLength(2,
                  errorText: formErrorMinLength(2)),
              FormBuilderValidators.maxLength(10,
                  errorText: formErrorMaxLength(10)),
            ]),
            keyboardType: TextInputType.text,
          ),
          const SizedBox(
            height: 10,
          ),
          Divider(),
          ComponentOutlineButton(
              callback: () {
                if (_formKey.currentState?.saveAndValidate() ?? false) {
                  MemberPutRequest request = MemberPutRequest(
                    _formKey.currentState!.fields['phoneNumber']!.value,
                    _formKey.currentState!.fields['finalEducation']!.value,
                    _formKey
                        .currentState!.fields['emergencyContactNumber']!.value,
                    _formKey
                        .currentState!.fields['emergencyContactName']!.value,
                    _formKey.currentState!
                        .fields['emergencyContactRelationship']!.value,
                    _formKey.currentState!.fields['address']!.value,
                  );
                  _putMember(request);
                }
              },
              name: '수정'),
        ],
      ),
    )));
  }
}
