import 'package:app_api_member_manager/component/component_appbar_actions.dart';
import 'package:app_api_member_manager/config/config_color.dart';
import 'package:app_api_member_manager/config/config_decoration.dart';
import 'package:app_api_member_manager/config/config_size.dart';
import 'package:app_api_member_manager/page/page_member_info.dart';
import 'package:flutter/material.dart';

class PageHome extends StatelessWidget {
  const PageHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: ComponentAppbarActions(
          title: '출근 다이어리',
          isUseActionBtn1: true,
          action1Icon: Icons.person,
          action1Callback: () {
            Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => PageMemberInfo())
            );
          }),
      body: _BuildBody(),
      backgroundColor: colorSecondary,
    );
  }


  Widget _BuildBody() {
    return Scaffold(
      body:
      Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
      Container(
      margin: bodyPaddingAll,
      child: Text('공지사항', style: TextStyle(color: colorDarkGray, fontSize: fontSizeSuper, fontWeight: FontWeight.bold) ),
    ),
    const SizedBox(
    height: 20,
    ),
    ]),
      backgroundColor: colorSecondary,
    );
  }
}

//todo 홈페이지 꾸미기
