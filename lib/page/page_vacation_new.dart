import 'package:app_api_member_manager/component/component_appbar_actions.dart';
import 'package:app_api_member_manager/component/component_custom_loading.dart';
import 'package:app_api_member_manager/component/component_notification.dart';
import 'package:app_api_member_manager/component/component_outline_button.dart';
import 'package:app_api_member_manager/config/config_color.dart';
import 'package:app_api_member_manager/config/config_decoration.dart';
import 'package:app_api_member_manager/config/config_dropdown.dart';
import 'package:app_api_member_manager/config/config_form_validator.dart';
import 'package:app_api_member_manager/model/vacation/vacation_apply_request.dart';
import 'package:app_api_member_manager/page/page_member_info.dart';
import 'package:app_api_member_manager/repository/repo_vacation.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';

class PageVacationNew extends StatefulWidget {
  const PageVacationNew({Key? key}) : super(key: key);

  @override
  State<PageVacationNew> createState() => _PageVacationNewState();
}

class _PageVacationNewState extends State<PageVacationNew> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _setVacation(VacationApplyRequest request) async {
    // 여기서부터
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    // 여기까지 커스텀로딩 띄우는 소스

    await RepoVacation().setVacation(request).then((res) {
      BotToast.closeAllLoading(); // 커스텀로딩 닫고

      ComponentNotification(
        success: true,
        title: '휴가 등록 성공',
        subTitle: res.msg, // 백엔드에서 반환받은 메세지 넣기
      ).call();


    }).catchError((err) {
      BotToast.closeAllLoading(); // 커스텀로딩 닫고

      ComponentNotification(
        success: false,
        title: '휴가 등록 실패',
        subTitle: '휴가 등록에 실패하였습니다.',
      ).call();
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarActions(
          title: '휴가등록',
          isUseActionBtn1: true,
          action1Icon: Icons.person,
          action1Callback: () {
            Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => PageMemberInfo())
            );
          }),
      body: _buildBody(),
      backgroundColor: colorSecondary,
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
        child: Container(
            padding: bodyPaddingAll,
            margin: bodyPaddingAll,
            decoration: BoxDecoration(
              color: colorWhite,
              borderRadius: BorderRadius.circular(10),
            ),
            child: FormBuilder(
              key: _formKey,
              autovalidateMode: AutovalidateMode.disabled,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  FormBuilderDateTimePicker(
                    name: 'dateRequest',
                    format: DateFormat('yyyy-MM-dd'),
                    inputType: InputType.date,
                    decoration: InputDecoration(
                      labelText: '신청일',
                      suffixIcon: IconButton(
                        icon: const Icon(Icons.close),
                        onPressed: () {
                          _formKey.currentState!.fields['dateRequest']
                              ?.didChange(null);
                        },
                      ),
                    ),
                    locale: const Locale.fromSubtags(languageCode: 'ko'),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(
                          errorText: formErrorRequired)
                    ]),
                  ),
                  FormBuilderDropdown<String>(
                    name: 'vacationName',
                    decoration: const InputDecoration(
                      labelText: '근태명',
                    ),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(
                          errorText: formErrorRequired),
                    ]),
                    items: dropdownVacationType,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  FormBuilderTextField(
                    name: 'timeStart',
                    decoration: const InputDecoration(
                        labelText: '시작시간', hintText: '2~10자 입력해주세요'),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(
                          errorText: formErrorRequired),
                      FormBuilderValidators.minLength(2,
                          errorText: formErrorMinLength(2)),
                      FormBuilderValidators.maxLength(10,
                          errorText: formErrorMaxLength(15)),
                    ]),
                    keyboardType: TextInputType.text,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  FormBuilderTextField(
                    name: 'timeEnd',
                    decoration: const InputDecoration(
                        labelText: '종료시간', hintText: '2~10자 입력해주세요'),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(
                          errorText: formErrorRequired),
                      FormBuilderValidators.minLength(2,
                          errorText: formErrorMinLength(2)),
                      FormBuilderValidators.maxLength(10,
                          errorText: formErrorMaxLength(15)),
                    ]),
                    keyboardType: TextInputType.text,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  FormBuilderTextField(
                    name: 'detail',
                    decoration: const InputDecoration(
                        labelText: '상세내역', hintText: '2~50자 입력해주세요'),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(
                          errorText: formErrorRequired),
                      FormBuilderValidators.minLength(2,
                          errorText: formErrorMinLength(2)),
                      FormBuilderValidators.maxLength(50,
                          errorText: formErrorMaxLength(15)),
                    ]),
                    keyboardType: TextInputType.text,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  ComponentOutlineButton(
                      callback: () {
                        if (_formKey.currentState?.saveAndValidate() ?? false) {
                          VacationApplyRequest request = VacationApplyRequest(
                            _formKey.currentState!.fields['timeStart']!.value,
                            _formKey.currentState!.fields['timeEnd']!.value,
                            DateFormat('yyyy-MM-dd').format(_formKey.currentState!.fields['dateRequest']!.value),
                            _formKey
                                .currentState!.fields['vacationName']!.value,
                            _formKey.currentState!.fields['detail']!.value,
                          );
                          _setVacation(request);
                        }
                      },
                      name: '제출'),
                ],
              ),
            )
        )
    );
  }
}
