import 'package:app_api_member_manager/config/config_color.dart';
import 'package:app_api_member_manager/page/page_work_time_record_check.dart';
import 'package:app_api_member_manager/page/page_vacation_list.dart';
import 'package:app_api_member_manager/page/page_vacation_new.dart';
import 'package:app_api_member_manager/page/page_work_time_record_list.dart';
import 'package:flutter/material.dart';



class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);



  @override
  State<StatefulWidget> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  static const _kTabPages = <Widget>[
    PageWorkTimeRecordCheck(),
    PageWorkTimeRecordList(),
    PageVacationNew(),
    PageVacationList(),



  ];
  static const _kTabs = <Tab>[
    Tab(icon: Icon(Icons.access_time, color: colorPrimary), child: Text('출근',style: TextStyle(color: colorPrimary),),),
    Tab(icon: Icon(Icons.calendar_today, color: colorPrimary), child: Text('출근조회',style: TextStyle(color: colorPrimary),),),
    Tab(icon: Icon(Icons.post_add, color: colorPrimary), child: Text('연차신청',style: TextStyle(color: colorPrimary),),),
    Tab(icon: Icon(Icons.airplanemode_active_outlined, color: colorPrimary), child: Text('연차조회',style: TextStyle(color: colorPrimary),),),
  ];

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      length: _kTabPages.length,
      vsync: this,
    );
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: TabBarView(
        controller: _tabController,
        children: _kTabPages,
      ),
      bottomNavigationBar: Material(
        color: colorWhite,
        child: TabBar(
          tabs: _kTabs,
          controller: _tabController,
        ),
      ),
    );
  }
}