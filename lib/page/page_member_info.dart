import 'package:app_api_member_manager/component/component_appbar_popup.dart';
import 'package:app_api_member_manager/component/component_custom_loading.dart';
import 'package:app_api_member_manager/component/component_member_text.dart';
import 'package:app_api_member_manager/component/component_notification.dart';
import 'package:app_api_member_manager/component/component_outline_button.dart';
import 'package:app_api_member_manager/config/config_color.dart';
import 'package:app_api_member_manager/config/config_decoration.dart';
import 'package:app_api_member_manager/functions/token_lib.dart';
import 'package:app_api_member_manager/model/member/member_detail.dart';
import 'package:app_api_member_manager/page/page_member_put.dart';
import 'package:app_api_member_manager/page/page_password_put.dart';
import 'package:app_api_member_manager/repository/repo_member.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PageMemberInfo extends StatefulWidget {
  const PageMemberInfo({Key? key}) : super(key: key);

  @override
  State<PageMemberInfo> createState() => _PageMemberInfoState();
}

class _PageMemberInfoState extends State<PageMemberInfo> {
  MemberDetail? _memberDetail;

  @override
  void initState() {
    _getMemberInfo();
    super.initState();
  }

  Future<void> _getMemberInfo() async {
    BotToast.showCustomLoading(
        toastBuilder: (cancelFunc) => ComponentCustomLoading(cancelFunc: cancelFunc)
    );

    await RepoMember().getMemberDetail().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _memberDetail = res.data;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '불러오기 실패',
        subTitle: '데이터 불러오기에 실패했습니다.',
      ).call();
    });
  }


  @override
  Widget build(BuildContext context) {
            return Scaffold(
      appBar: ComponentAppbarPopup(
          title: '회원정보',
          ),
      body: _buildBody(),
      backgroundColor: colorSecondary,
      bottomNavigationBar: BottomAppBar(
        child: ComponentOutlineButton(
          callback: () {_showLogoutDialog();},
          name: '로그아웃',
        ),
      ),
    );
  }

  void _showLogoutDialog() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text("로그아웃"),
            content: Container(
                child: const Text('정말로 로그아웃하시겠습니까?')
            ),
            actions: [
              CupertinoButton(
                onPressed: () {
                  TokenLib.logout(context);
                },
                child: const Text("로그아웃"),
              ),
              CupertinoButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text("닫기"),
              ),
            ],
          );
        }
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
        child: Container(
          padding: bodyPaddingAll,
          margin: bodyPaddingAll,
          decoration: BoxDecoration(
            color: colorWhite,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              ComponentMemberText(name: '아이디', variableName: _memberDetail?.username ?? '-'),
              ComponentMemberText(name: '생년월일', variableName: _memberDetail?.birthday ?? '-'),
              ComponentMemberText(name: '부서', variableName: _memberDetail?.department ?? '-'),
              ComponentMemberText(name: '직책', variableName: _memberDetail?.position ?? '-'),
              ComponentMemberText(name: '입사일자', variableName: _memberDetail?.dateJoin ?? '-'),
              ComponentMemberText(name: '전화번호', variableName: _memberDetail?.phoneNumber ?? '-'),
              ComponentMemberText(name: '최종학력', variableName: _memberDetail?.username ?? '-'),
              ComponentMemberText(name: '집주소', variableName: _memberDetail?.address ?? '-'),
              ComponentMemberText(name: '비상연락망', variableName: _memberDetail?.emergencyContactNumber ?? '-'),
              ComponentMemberText(name: '비상연락망 성함', variableName: _memberDetail?.emergencyContactName ?? '-'),
              ComponentMemberText(name: '비상연락망 관계', variableName: _memberDetail?.emergencyContactRelationship ?? '-'),

              Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ComponentOutlineButton(
                  callback: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PagePasswordPut()));
                  },
                  name: '비밀번호수정'),
              ComponentOutlineButton(
                  callback: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PageMemberPut()));
                  },
                  name: '정보수정'),
            ],
          ),
        ],
          ),
        )
    );
  }
}
