import 'package:app_api_member_manager/component/component_appbar_actions.dart';
import 'package:app_api_member_manager/component/component_custom_loading.dart';
import 'package:app_api_member_manager/component/component_member_text.dart';
import 'package:app_api_member_manager/component/component_notification.dart';
import 'package:app_api_member_manager/component/component_outline_button.dart';
import 'package:app_api_member_manager/config/config_color.dart';
import 'package:app_api_member_manager/config/config_decoration.dart';
import 'package:app_api_member_manager/config/config_size.dart';
import 'package:app_api_member_manager/page/page_member_info.dart';
import 'package:app_api_member_manager/repository/repo_work_time_record.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';

class PageWorkTimeRecordCheck extends StatefulWidget {
  const PageWorkTimeRecordCheck({Key? key}) : super(key: key);

  @override
  State<PageWorkTimeRecordCheck> createState() =>
      _PageWorkTimeRecordCheckState();
}

class _PageWorkTimeRecordCheckState extends State<PageWorkTimeRecordCheck> {
  String workStateName = '-';
  String dateStart = '-';
  String dateEarlyLeave = '-';
  String dateEnd = '-';

  String _workState = 'NONE';

  @override
  void initState() {
    _getMyState();
    super.initState();
  }

  Future<void> _getMyState() async {
    BotToast.showCustomLoading(
        toastBuilder: (cancelFunc) =>
            ComponentCustomLoading(cancelFunc: cancelFunc));

    await RepoWorkTimeRecord().getMyState().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        workStateName = res.data.workStateName;
        dateStart = res.data.dateStart ?? '-';
        dateEarlyLeave = res.data.dateEarlyLeave ?? '-';
        dateEnd = res.data.dateEnd ?? '-';
        _workState = res.data.workState;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '불러오기 실패',
        subTitle: '데이터 불러오기에 실패했습니다.',
      ).call();
    });
  }

  Future<void> _putWorkTimeRecord(String workState) async {
    BotToast.showCustomLoading(
        toastBuilder: (cancelFunc) =>
            ComponentCustomLoading(cancelFunc: cancelFunc));

    await RepoWorkTimeRecord().putWorkTimeRecord(workState).then((res) {
      BotToast.closeAllLoading();

      if (res.isSuccess) {
        _getMyState();
      } else {
        ComponentNotification(
          success: false,
          title: '상태 변경 실패',
          subTitle: res.msg,
        ).call();
      }
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '상태 변경 실패',
        subTitle: '상태 변경에 실패했습니다.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarActions(
          title: '출퇴근등록',
          isUseActionBtn1: true,
          action1Icon: Icons.person_pin,
          action1Callback: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => PageMemberInfo()));
          }),
      body: _buildBody(),
      backgroundColor: colorSecondary,
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
        child: Container(
      padding: bodyPaddingAll,
      margin: bodyPaddingAll,
      decoration: BoxDecoration(
        color: colorWhite,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ComponentMemberText(name: '현재 상태 : ', variableName: workStateName),
          ComponentMemberText(name: '출근 시간 : ', variableName: dateStart),
          ComponentMemberText(name: '조퇴 시간 : ', variableName: dateEarlyLeave),
          ComponentMemberText(name: '퇴근 시간 : ', variableName: dateEnd),
          SizedBox(
            height: sizeBodyHeight,
          ),
          if (_workState == 'NONE')
            ComponentOutlineButton(
                callback: () {
                  _putWorkTimeRecord('COMPANY_IN');
                },
                name: '출근'),
          if (_workState == 'COMPANY_IN')
            ComponentOutlineButton(
                callback: () {
                  _putWorkTimeRecord('EARLY_LEAVE');
                },
                name: '조퇴'),
          SizedBox(
            height: sizeBodyHeight,
          ),
          if (_workState == 'COMPANY_IN')
            ComponentOutlineButton(
                callback: () {
                  _putWorkTimeRecord('COMPANY_OUT');
                },
                name: '퇴근'),
          if (_workState == 'EARLY_LEAVE' || _workState == 'COMPANY_OUT')
            Image.asset(
              'assets/COMPANY_OUT.png',
              width: 200,
              height: 200,
            ),
          if (_workState == 'EARLY_LEAVE' || _workState == 'COMPANY_OUT')
          Text('오늘 하루 수고하셨습니다', textAlign: TextAlign.center)
        ],
      ),
    ));
  }
}
