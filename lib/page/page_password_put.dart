import 'package:app_api_member_manager/component/component_appbar_popup.dart';
import 'package:app_api_member_manager/component/component_custom_loading.dart';
import 'package:app_api_member_manager/component/component_notification.dart';
import 'package:app_api_member_manager/component/component_outline_button.dart';
import 'package:app_api_member_manager/config/config_color.dart';
import 'package:app_api_member_manager/config/config_decoration.dart';
import 'package:app_api_member_manager/config/config_form_validator.dart';
import 'package:app_api_member_manager/model/member/member_password_change_request.dart';
import 'package:app_api_member_manager/repository/repo_member.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PagePasswordPut extends StatefulWidget {
   PagePasswordPut({super.key, this.changeRequest});

   MemberPasswordChangeRequest? changeRequest;

  @override
  State<PagePasswordPut> createState() => _PagePasswordPutState();
}

class _PagePasswordPutState extends State<PagePasswordPut> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _putMyPassword(MemberPasswordChangeRequest changeRequest) async {
    BotToast.showCustomLoading(
        toastBuilder: (cancelFunc) => ComponentCustomLoading(cancelFunc: cancelFunc)
    );

    await RepoMember().putPassword(changeRequest).then((res) {
      BotToast.closeAllLoading();


      if (res.isSuccess) {
        ComponentNotification(
          success: true,
          title: '비밀번호 변경 성공',
          subTitle: '비밀번호 변경에 성공했습니다.',
        ).call();

        Navigator.pop(context, [true],);
      } else {
        ComponentNotification(
          success: false,
          title: '비밀번호 변경 실패',
          subTitle: res.msg,
        ).call();
      }

    }).catchError((err) {
      BotToast.closeAllLoading();
      ComponentNotification(
        success: false,
        title: '비밀번호 변경 실패',
        subTitle: '비밀번호 변경에 실패했습니다.',
      ).call();
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(
        title: '비밀번호 수정'),
        body: _buildBody(),
      backgroundColor: colorSecondary,
    );
  }


  Widget _buildBody() {
    return SingleChildScrollView(
        child: Container(
      padding: bodyPaddingAll,
      margin: bodyPaddingAll,
      decoration: BoxDecoration(
        color: colorWhite,
        borderRadius: BorderRadius.circular(10),
      ),
      child: FormBuilder(
      key: _formKey,
      autovalidateMode: AutovalidateMode.disabled,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          FormBuilderTextField(
            name: 'password',
            decoration: const InputDecoration(
              labelText: '비밀번호',
                hintText: '8~20자 입력해주세요'
            ),
            obscureText: true,
            validator: FormBuilderValidators.compose([
              FormBuilderValidators.required(errorText: formErrorRequired),
              FormBuilderValidators.minLength(8,
                  errorText: formErrorMinLength(8)),
              FormBuilderValidators.maxLength(20,
                  errorText: formErrorMaxLength(20)),
            ]),
            keyboardType: TextInputType.text,
          ),
          FormBuilderTextField(
            name: 'newPassword',
            decoration: const InputDecoration(
              labelText: '새 비밀번호',
                hintText: '8~20자 입력해주세요'
            ),
            obscureText: true,
            validator: FormBuilderValidators.compose([
              FormBuilderValidators.required(errorText: formErrorRequired),
              FormBuilderValidators.minLength(8,
                  errorText: formErrorMinLength(8)),
              FormBuilderValidators.maxLength(20,
                  errorText: formErrorMaxLength(20)),
            ]),
            keyboardType: TextInputType.text,
          ),
          FormBuilderTextField(
            name: 'newPasswordConfirm',
            decoration: const InputDecoration(
              labelText: '비밀번호 확인',
                hintText: '8~20자 입력해주세요'
            ),
            obscureText: true,
            validator: FormBuilderValidators.compose([
              FormBuilderValidators.required(errorText: formErrorRequired),
              FormBuilderValidators.minLength(8,
                  errorText: formErrorMinLength(8)),
              FormBuilderValidators.maxLength(20,
                  errorText: formErrorMaxLength(20)),
            ]),
            keyboardType: TextInputType.text,
          ),
          const SizedBox(
            height: 10,
          ),
          Divider(),
          ComponentOutlineButton(
              callback: () {
                if (_formKey.currentState?.saveAndValidate() ?? false) {
                  MemberPasswordChangeRequest changeRequest = MemberPasswordChangeRequest(
                    _formKey.currentState!.fields['password']!.value,
                    _formKey.currentState!.fields['newPassword']!.value,
                    _formKey.currentState!.fields['newPasswordConfirm']!.value,
                  );
                  _putMyPassword(changeRequest);
                }
              },
              name: '변경'),

        ],
      ),
      )));
  }
}
