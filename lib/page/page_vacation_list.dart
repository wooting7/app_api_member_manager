import 'package:app_api_member_manager/component/component_appbar_actions.dart';
import 'package:app_api_member_manager/component/component_count_title.dart';
import 'package:app_api_member_manager/component/component_custom_loading.dart';
import 'package:app_api_member_manager/component/component_data_table.dart';
import 'package:app_api_member_manager/component/component_vacation_list_item.dart';
import 'package:app_api_member_manager/component/component_outline_button.dart';
import 'package:app_api_member_manager/config/config_color.dart';
import 'package:app_api_member_manager/config/config_decoration.dart';
import 'package:app_api_member_manager/config/config_dropdown.dart';
import 'package:app_api_member_manager/model/vacation/vacation_item.dart';
import 'package:app_api_member_manager/page/page_member_info.dart';
import 'package:app_api_member_manager/repository/repo_vacation.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class PageVacationList extends StatefulWidget {
  const PageVacationList({Key? key}) : super(key: key);

  @override
  State<PageVacationList> createState() => _PageVacationListState();
}

class _PageVacationListState extends State<PageVacationList> {

  int _selectedYear = DateTime
      .now()
      .year;
  int _selectedMonth = DateTime
      .now()
      .month;

  List<VacationItem> _list = [];
  int _totalItemCount = 0;




  Future<void> _loadItems() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoVacation()
        .getMyVacationList(_selectedYear, _selectedMonth)
        .then((res) => {
      BotToast.closeAllLoading(),
      setState(() {
        _totalItemCount = res.totalItemCount;
        _list = res.list;
      })
    })
        .catchError((err) => {
      BotToast.closeAllLoading(),
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child:
        Scaffold(
          backgroundColor: colorSecondary,
          appBar: ComponentAppbarActions(
              title: '휴가조회',
              isUseActionBtn1: true,
              action1Icon: Icons.person,
              action1Callback: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            PageMemberInfo(
                            )));
              }),
          bottomNavigationBar: BottomAppBar(
            child: ComponentOutlineButton(
              callback: () {
                _loadItems();
              },
              name: '조회하기',
            ),
          ),
          body: SingleChildScrollView(
            child: Container(
              padding: bodyPaddingAll,
              margin: bodyPaddingAll,
              decoration: BoxDecoration(color: colorWhite,
                borderRadius: BorderRadius.circular(10),),
              child: Column(
                children: [
                  FormBuilderDropdown<int>(
                    name: 'year',
                    decoration: const InputDecoration(
                      labelText: '년도',
                    ),
                    items: dropdownYear,
                    initialValue: _selectedYear,
                    onChanged: (val) {
                      setState(() {
                        _selectedYear = val!;
                      });
                      _loadItems();
                    },
                  ),
                  SizedBox(width: 10),
                  FormBuilderDropdown<int>(
                    name: 'month',
                    decoration: const InputDecoration(
                      labelText: '월',
                    ),
                    items: dropdownMonth,
                    initialValue: _selectedMonth,
                    onChanged: (val) {
                      setState(() {
                        _selectedMonth = val!;
                      });
                      _loadItems();

                    },
                  ),
                  SizedBox(height: 10,),
                  ComponentCountTitle(icon: Icons.add, count: _totalItemCount, unitName: '건', itemName: '휴가 데이터'),
                  Divider(),
                  Row(
                    children: [
                      SizedBox(width: 30,),
                      Text('날짜'),
                      SizedBox(width: 48,),
                      Text('근태'),
                      SizedBox(width: 30,),
                      Text('결제'),
                      SizedBox(width: 30,)
                    ],
                  ),
                  SizedBox(height: 10,),
                  Divider(),
                  _buildBody(),
                ],
              ),
            ),
          ),
        )
    );
  }

  Widget _buildBody() {
    return Column(
      children: [

        ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) => ComponentVacationListItem(item: _list[index])
        )
      ],
    );
  }
}


