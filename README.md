# 사원관리 APP
***
소개: 직원들의 출퇴근 관리를 위한 앱 입니다
***
#### * 목차 *
```
1. 언어
2. 기능
3. APP 이미지
```
#### 1. 언어
```
DART
FLUTTER
```
#### 2. 기능
```
* 사원관리
  - (사용자) 사원 등록_2nd
  - (사용자) 사원 비밀번호 변경
  - (사용자) 로그인 
  - (사용자) 사원 정보 조회
    
* 연차관리
  - (사용자) 연차 등록
  - (사용자) 연차 조회
  
* 출퇴근관리
  - (사용자) 사원 출퇴근 현황 조회
  - (사용자) 사원 출퇴근 상태 수정
  - (사용자) 사원 출퇴근 월별 조회
  
```

#### 3. APP 이미지

>* 로그인
>
>![app_login](./assets/app_login.png)

>* 출퇴근등록
>
>![app_work_in_out](./assets/app_work_in_out.png)

>* 출퇴근기록
>
>![app_work_in_out_list](./assets/app_work_in_out_list.png)

>* 휴가등록
>
>![app_vacation_save](./assets/app_vacation_save.png)

>* 회원정보 & 수정
>
>![app_vacation_list](./assets/app_vacation_list.png)

>* 비밀번호 수정
>
>![app_member_info](./assets/app_member_info.png)

>* 로그아웃
>
>![app_member_password_update](./assets/app_member_password_update.png)

>* 로그아웃
>
>![app_logout](./assets/app_logout.png)
